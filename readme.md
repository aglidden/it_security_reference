A list of security info useful for remembering where everything is and answering netsec questions.  

The road most traveled to get into info/netsec: helpdesk -> sysadmin -> security  

#Tools:
Link   | Description
-------|---------------
[SecTools](http://sectools.org/)							| Top 125 Network Security Tools
[Kali](https://www.kali.org/)      							| Popular pentesting linux distro
[Tails](https://tails.boum.org/)							| Privacy linux distro, forces everything through Tor
[babun](https://babun.github.io/)                           | Windows ZSH

#Pentesting: 
Link   | Description
-------|---------------
[Vulnhub](https://www.vulnhub.com/)                                                                | Cap the flag VMs
[OverTheWire](https://overthewire.org/wargames/)                                                   | OS/Web wargames (over ssh/http)
[WebGoat](https://github.com/WebGoat/WebGoat)                                                      | vulnerable web app
[Metasploitable/Unleashed](https://www.offensive-security.com/metasploit-unleashed/requirements/)  | class on metasploit and VM made for metasploit
[SmashTheStack](http://smashthestack.org/)                                                         | Software vulnerability wargames
[HackThisSite](https://www.hackthissite.org/)                                                      | Various wargames

#Knowledge

##Free Security Videos: 
Link   | Description   
-------|---------------
[Securitytube](http://www.securitytube.net/)                                                       | collection of security focused youtube videos
[ceos3c](https://www.ceos3c.com/)                                                                  | collection of IT videos, some security focused

##Security RSS Feeds: 
Link   | Description
-------|---------------
[Naked Security](https://nakedsecurity.sophos.com/)        | sophos' firehose
[Krebs](http://krebsonsecurity.com/)                       | Brian Krebs' blog
[ISC](https://isc.sans.edu/xml.html)                       | SANS Internet Storm Center, blog/podcasts
[Schneier](https://www.schneier.com/)                      | Bruce Schneier's blog
[US-CERT weekly](https://www.us-cert.gov/ncas/bulletins)   | US-CERT weekly summary of vulnerabilities
[SecLists](http://seclists.org/)                           | A billion rss feeds for mailing lists